*** Settings ***
Documentation    Test suite to verify BGP session establishment
Suite Setup       Setup Config
Suite Teardown    Setup Teardown
Test Teardown     Teardown Test Config
Library           SSHLibrary
Resource          variables.robot
Resource          resource.robot


*** Test Cases ***
Verify Bgp Session With Same Password
    [Documentation]    Test to verify bgp session establishment with same password configured on peers
    Switch Connection    ${R1_ALIAS}
    Bgp Config    ${R1_BGP_AS}    ${R1_INT_IP}    ${R1_NEIGHBOUR}    ${R1_BGP_AS}    ${BGP_PASSWORD1}
    ${Output}    Execute Command    show running-config
    Log    '${R1_ALIAS} running config is \n${Output}'    console=yes
    Switch Connection    ${R2_ALIAS}
    Bgp Config    ${R2_BGP_AS}    ${R2_INT_IP}    ${R2_NEIGHBOUR}    ${R2_BGP_AS}    ${BGP_PASSWORD1}
    Log    '${R2_ALIAS} running config is \n${Output}'    console=yes
    # Polling for the desired BGP session state
    FOR    ${Index}    IN RANGE    10
        ${Response}    Execute Command    show bgp sessions
        Exit For Loop If    'Established' in ${Response}
        Sleep    5s
    END
    Log    "/nBgp session response is ${Response}"    console=yes
    Should Contain    ${Response}    Established    ignore_case=True    msg=Failed to establish Bgp session.
    Log    "\nBgp session got successfully established"    console=yes

Verify Bgp Session With Different Password
    [Documentation]    Test to verify bgp session establishment with different password configured on peers
    Switch Connection    ${R1_ALIAS}
    Bgp Config    ${R1_BGP_AS}    ${R1_INT_IP}    ${R1_NEIGHBOUR}    ${R1_BGP_AS}    ${BGP_PASSWORD1}
    Log    '${R1_ALIAS} running config is \n${Output}'    console=yes
    Switch Connection    ${R2_ALIAS}
    Bgp Config    ${R2_BGP_AS}    ${R2_INT_IP}    ${R2_NEIGHBOUR}    ${R2_BGP_AS}    ${BGP_PASSWORD2}
    Log    '${R2_ALIAS} running config is \n${Output}'    console=yes
    sleep    30s
    ${Response}    Execute Command    show bgp sessions
    Log    "/nBgp session response is ${Response}"    console=yes
    Should Not Contain    ${Response}    'Established'    ignore_case=True    msg=Bgp session got established
    Log    "\nNo Bgp session established as expected"    console=yes


*** Keywords ***
Bgp Config
    [Arguments]    ${BgpAs}    ${RouterId}    ${Neighbour}    ${RemoteAs}    ${Password}
    [Documentation]    Keyword for router BGP configuration
    Execute Command    conf terminal
    Execute Command    router bgp ${BgpAs}
    Execute Command    bgp router-id ${RouterId}
    Execute Command    address-family ipv4 unicast
    Execute Command    neighbor ${Neighbour}
    Execute Command    remote-as ${RemoteAs}
    Execute Command    password ${Password}
    ${Response}    Execute Command    commit    timeout=20
    Should Not Contain    ${Response}    Failed    ignore_case=True    msg=Failed to commit the config
    Execute Command    end
    Execute Command    conf terminal
    Execute Command    router bgp ${BgpAs}
    Execute Command    neighbor ${Neighbour}
    Execute Command    address-family ipv4 unicast maximum-prefix 1000
    ${Response1}    Execute Command    commit    timeout=10
    Should Not Contain    ${Response1}    Failed    ignore_case=True    msg=Failed to commit the config
    Log    "\nBGP session configuration is successful"    console=yes
    Execute Command    end

Delete Bgp Config
    [Documentation]    Deleting BGP configuration.
    [Arguments]    ${BgpAs}    ${ConnectionId}
    Switch Connection    ${ConnectionId}
    Execute Command    conf terminal
    Execute Command    no router bgp ${BgpAs}
    ${Response}    Execute Command    commit    timeout=20
    Should Not Contain    ${Response}    Failed    ignore_case=True    msg=Failed to commit the config
    Execute Command    end
    Log    "\nBGP config deletion is successful"    console=yes

Teardown Test Config
    [Documentation]    Deleting test configuration after test completed
    Delete Bgp Config    ${R1_BGP_AS}    ${R1_ALIAS}
    Delete Bgp Config    ${R2_BGP_AS}    ${R2_ALIAS}
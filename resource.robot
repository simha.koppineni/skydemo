*** Settings ***
Documentation    Resource file for common keywords used across multiple test suites.
Library     SSHLibrary
Resource    variables.robot


*** Keywords ***
Device Login
    [Arguments]    ${DeviceIp}    ${SshPort}    ${UserName}    ${Password}    ${ConnectionId}    ${Prompt}
    [Documentation]    Keyword for logging into a device using SSH
    Open Connection    ${DeviceIp}    port=${SshPort}    prompt=${Prompt}    alias=${ConnectionId}
    ${Response}    Run Keyword    Login    ${UserName}    ${Password}
    Should be true    ${Response}==True    msg='Device login failed over SSH'
    Set Suite Variable    ${ConnectionId}
    Log    "\nDevice login is successful"    console=yes

Interface Config
    [Arguments]    ${InterfaceIp}    ${Interface}    ${ConnectionId}
    [Documentation]    Keyword for enabling device interface and configuring with Ip address
    # Switch Connection    ${ConnectionId}
    Execute Command    configure terminal
    Execute Command    int ${Interface}
    Execute Command    no shutdown
    Execute Command    ip address ${InterfaceIp}/24
    ${Response}    Execute Command    commit    timeout=10
    Should Not Contain    ${Response}    Failed    ignore_case=True    msg=Failed to commit the config
    Log    "\nInterface configuration is successful"    console=yes
    Execute Command    end

Delete Interface Config
    [Arguments]    ${Interface}    ${ConnectionId}
    [Documentation]    Keyword for enabling device interface and configuring with Ip address
    Switch Connection    ${ConnectionId}
    Execute Command    configure terminal
    Execute Command    int ${Interface}
    Execute Command    no ip address
    Execute Command    shutdown
    ${Response}    Execute Command    commit    timeout=10
    Should Not Contain    ${Response}    Failed    ignore_case=True    msg=Failed to commit the config
    Log    "\nInterface config deletion is successful"    console=yes
    Execute Command    end

Setup Config
    [Documentation]    Suite setup keyword for initial router configuration
    Device Login    ${DEVICE_IP}    ${R1_PORT}    ${USER_NAME}    ${DEVICE_PWD}    ${R1_ALIAS}    ${PROMPT1}
    Interface Config    ${R1_INT_IP}    ${INTERFACE}    ${R1_ALIAS}
    Device Login    ${DEVICE_IP}    ${R2_PORT}    ${USER_NAME}    ${DEVICE_PWD}    ${R2_ALIAS}    ${PROMPT2}
    Interface Config    ${R2_INT_IP}    ${INTERFACE}    ${R2_ALIAS}

Setup Teardown
    [Documentation]    Suite teardown keyword for deleting router config
    Delete Interface Config    ${INTERFACE}    ${R1_ALIAS}
    Delete Interface Config    ${INTERFACE}    ${R2_ALIAS}
    Close All Connections
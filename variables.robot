*** Settings ***
Documentation    Collection of variable used in BGP test suite

*** Variables ***
${DEVICE_IP}        10.10.20.70
${R1_PORT}          2221
${R2_PORT}          2231
${USER_Name}        admin
${DEVICE_PWD}       admin
${R1_INT_IP}        172.168.1.10
${R2_INT_IP}        172.168.1.20
${R1_BGP_AS}        10
${R2_BGP_AS}        20
${PROMPT1}          RP/0/RP0/CPU0:r1#
${PROMPT2}          RP/0/RP0/CPU0:r2#
${R1_ALIAS}         Router1
${R2_ALIAS}         Router2
${INTERFACE}        g0/0/0/0
${R1_NEIGHBOUR}     172.168.1.20
${R2_NEIGHBOUR}     172.168.1.10
${BGP_PASSWORD1}    test1
${BGP_PASSWORD2}    test2